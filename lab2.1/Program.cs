﻿using System;

namespace lab
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string text;
            int num;
            Console.Write("Enter text: ");
            text = Console.ReadLine();

            while (true)
            {
                try
                {
                    Console.Write("Enter number: ");
                    string line = Console.ReadLine();
                    num = Convert.ToInt32(line);
                    break;
                }
                catch (FormatException e)
                {
                    Console.Write("Error, it is not an integer! ");
                }
            }

            Console.WriteLine($"Your output: {num - text.Length}");
        }
    }
}